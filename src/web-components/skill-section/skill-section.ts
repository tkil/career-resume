import { html, render } from "lit-html";
import skills from "@data/skills";

const componentName = "skill-section";

const reduceToSuperCategories = (skills: any[]) => {
  const superCats = skills.reduce((acc, curr) => {
    if (curr.superCategory && acc.indexOf(curr.superCategory) === -1) {
      acc.push(curr.superCategory);
    }
    return acc;
  }, []);
  return superCats.map((sc: any) => {
    return {
      superCategory: sc,
      skills: skills.filter(s => s.superCategory === sc)
    };
  });
};

const reduceToCategories = (skills: any[], superCategory: string) => {
  const cats = skills.reduce((acc, curr) => {
    if (curr.category && acc.indexOf(curr.category) === -1) {
      acc.push(curr.category);
    }
    return acc;
  }, []);
  return cats.map((c: any) => {
    return {
      category: c,
      skills: skills.filter(s => s.category === c && s.superCategory === superCategory)
    };
  });
};

const style = /*css*/ `
  .skill-section {

  }
  .skill-section__super-category {
    margin: 16px auto 24px;
  }
  .skill-section__super-category-title.skill-section__super-category-title {
    margin-bottom: 0;
    text-align: center;
  }
`;

const template = (superCategories: any[]) => {
  return html`
    <style>
      ${style}
    </style>
    <div class="skill-section">
      ${superCategories.map((sc: any) => {
        const cats = reduceToCategories(sc.skills, sc.superCategory);
        console.log({ cats });
        return html`
          <div class="skill-section__super-category">
            <hr />
            <h4 class="skill-section__super-category-title title is-italic is-4">${sc.superCategory}</h4>
            ${cats.map((c: any) => {
              console.log({ c });
              return html`
                <skill-category data-category-skills="${JSON.stringify(c.skills)}"></skill-category>
              `;
            })}
          </div>
        `;
      })}
    </div>
  `;
};

// class MyComponent extends HTMLElement { ... }
// window.customElements.define('my-component', MyComponent);
window.customElements.define(
  componentName,
  class extends HTMLElement {
    private _superCategories: any;

    constructor() {
      super();
      // Attribute Mapping
      this._superCategories = reduceToSuperCategories(skills);
      // An instance of the element is created or upgraded.
      // Useful for initializing state, setting up event listeners, or creating a shadow dom.
      // See the spec for restrictions on what you can do in the constructor.
    }

    public connectedCallback() {
      render(template(this._superCategories), this);
      // Called every time the element is inserted into the DOM.
      // Useful for running setup code, such as fetching resources or rendering.
      // Generally, you should try to delay work until this time.
    }

    public disconnectedCallback() {
      // Called every time the element is removed from the DOM. Useful for running clean up code.
    }

    public static get observedAttributes() {
      return ["attr"];
      // Elements can react to attribute changes by defining a attributeChangedCallback.
      // The browser will call this method for every change to attributes listed in the observedAttributes array.
    }

    public adoptedCallback() {
      // Called when an observed attribute has been added, removed, updated, or replaced.
      // Also called for initial values when an element is created by the parser, or upgraded.
      // Note: only attributes listed in the observedAttributes property will receive this callback.
      // The adoptedCallback is called each time the custom element is moved to a new document.
      // You'll only run into this use case when you have <iframe> elements in your page.
    }
  }
);
