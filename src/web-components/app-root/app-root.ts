import { html, render } from "lit-html";

const componentName = "app-root";

const style = /*css*/ `
  .app-root {
    margin: 0 auto;
    padding: 24px 16px;
    max-width: 600px;
    background-color: #ffffff;
  }
`;

const template = () => html`
  <style>
    ${style}
  </style>
  <div class="app-root">
    <section class="">
      <div class="container">
        <h1 class="title has-text-centered">
          Tyler Kilburn
        </h1>
        <p class="subtitle is-italicized has-text-centered">Full Stack Developer - Never stop learning</p>
        <div class="has-text-centered">
          <p class="is-italicized">Talk is cheap, show me the code!</p>
          <p class="is-italicized">- Linus Torvalds</p>
        </div>
      </div>
    </section>
    <achievements-section></achievements-section>
    <skill-section></skill-section>
  </div>
`;

window.customElements.define(
  componentName,
  class extends HTMLElement {
    private _attr: string;

    constructor() {
      super();
      // Attribute Mapping
      this._attr = this.getAttribute("attr") || "";
      // An instance of the element is created or upgraded.
      // Useful for initializing state, setting up event listeners, or creating a shadow dom.
      // See the spec for restrictions on what you can do in the constructor.
    }

    public connectedCallback() {
      render(template(), this);
      // Called every time the element is inserted into the DOM.
      // Useful for running setup code, such as fetching resources or rendering.
      // Generally, you should try to delay work until this time.
    }

    public disconnectedCallback() {
      // Called every time the element is removed from the DOM. Useful for running clean up code.
    }

    public static get observedAttributes() {
      return ["attr"];
      // Elements can react to attribute changes by defining a attributeChangedCallback.
      // The browser will call this method for every change to attributes listed in the observedAttributes array.
    }

    public attributeChangedCallback(attrName: string, oldVal: string, newVal: string) {
      switch (attrName) {
        case "attr":
          this._attr = newVal;
          return;
      }
    }

    public adoptedCallback() {
      // Called when an observed attribute has been added, removed, updated, or replaced.
      // Also called for initial values when an element is created by the parser, or upgraded.
      // Note: only attributes listed in the observedAttributes property will receive this callback.
      // The adoptedCallback is called each time the custom element is moved to a new document.
      // You'll only run into this use case when you have <iframe> elements in your page.
    }
  }
);
