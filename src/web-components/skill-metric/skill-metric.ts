import { html, render } from "lit-html";

const componentName = "skill-metric";

const style = /*css*/ `
  .skill-metric {
    margin: 8px;
  }
`;

const template = (label: string, score: number) => {
  let colorClass = "is-warning";
  switch (true) {
    case score >= 8:
      colorClass = "is-link";
      break;
    case score >= 6:
      colorClass = "is-primary";
      break;
    case score >= 4:
      colorClass = "is-success";
      break;
  }
  return html`
    <style>
      ${style}
    </style>
    <div class="skill-metric">
      <label class="label">${label}</label>
      <progress class="progress ${colorClass}" value="${score}" max="10"></progress>
    </div>
  `;
};

window.customElements.define(
  componentName,
  class extends HTMLElement {
    private _score: number;
    private _skill: string;

    constructor() {
      super();
      // Attribute Mapping
      this._score = Number.parseInt(this.getAttribute("data-score") || "0");
      this._skill = this.getAttribute("data-skill") || "";
      // An instance of the element is created or upgraded.
      // Useful for initializing state, setting up event listeners, or creating a shadow dom.
      // See the spec for restrictions on what you can do in the constructor.
    }

    public connectedCallback() {
      render(template(this._skill, this._score), this);
      // Called every time the element is inserted into the DOM.
      // Useful for running setup code, such as fetching resources or rendering.
      // Generally, you should try to delay work until this time.
    }

    public disconnectedCallback() {
      // Called every time the element is removed from the DOM. Useful for running clean up code.
    }

    public static get observedAttributes() {
      return ["data-score", "data-skill"];
      // Elements can react to attribute changes by defining a attributeChangedCallback.
      // The browser will call this method for every change to attributes listed in the observedAttributes array.
    }

    public attributeChangedCallback(attrName: string, _: string, newVal: string) {
      switch (attrName) {
        case "data-score":
          this._score = Number.parseInt(newVal || "0");
          return;
        case "data-skill":
          this._skill = newVal;
          return;
      }
    }

    public adoptedCallback() {
      // Called when an observed attribute has been added, removed, updated, or replaced.
      // Also called for initial values when an element is created by the parser, or upgraded.
      // Note: only attributes listed in the observedAttributes property will receive this callback.
      // The adoptedCallback is called each time the custom element is moved to a new document.
      // You'll only run into this use case when you have <iframe> elements in your page.
    }
  }
);
