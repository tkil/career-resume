import { html, render } from "lit-html";
import achievements from "@data/achievements";

const componentName = "achievements-section";

const style = /*css*/ `
  .achievements-section {
    margin: 24px;
  }

  .achievements-section__title {

  }

  .achievements-section__list {
    list-style-type: disc;
  }
`;

const template = () => html`
  <style>
    ${style}
  </style>
  <div class="achievements-section">
    <h4 class="achievements-section__title title is-4 is-italic has-text-centered">Achievements and Qualities</h4>
    <ul class="achievements-section__list">
      ${achievements.map((a: any) => {
        return html`
          <li>${a}</li>
        `;
      })}
    </ul>
  </div>
`;

// class MyComponent extends HTMLElement { ... }
// window.customElements.define('my-component', MyComponent);
window.customElements.define(
  componentName,
  class extends HTMLElement {
    private _attr: string;

    constructor() {
      super();
      // Attribute Mapping
      this._attr = this.getAttribute("attr") || "";
      // An instance of the element is created or upgraded.
      // Useful for initializing state, setting up event listeners, or creating a shadow dom.
      // See the spec for restrictions on what you can do in the constructor.
    }

    public connectedCallback() {
      render(template(), this);
      // Called every time the element is inserted into the DOM.
      // Useful for running setup code, such as fetching resources or rendering.
      // Generally, you should try to delay work until this time.
    }

    public disconnectedCallback() {
      // Called every time the element is removed from the DOM. Useful for running clean up code.
    }

    public static get observedAttributes() {
      return ["attr"];
      // Elements can react to attribute changes by defining a attributeChangedCallback.
      // The browser will call this method for every change to attributes listed in the observedAttributes array.
    }

    public attributeChangedCallback(attrName: string, oldVal: string, newVal: string) {
      switch (attrName) {
        case "attr":
          this._attr = newVal;
          return;
      }
    }

    public adoptedCallback() {
      // Called when an observed attribute has been added, removed, updated, or replaced.
      // Also called for initial values when an element is created by the parser, or upgraded.
      // Note: only attributes listed in the observedAttributes property will receive this callback.
      // The adoptedCallback is called each time the custom element is moved to a new document.
      // You'll only run into this use case when you have <iframe> elements in your page.
    }
  }
);
