import { html, render } from "lit-html";

const componentName = "skill-category";

const style = /*css*/ `
  .skill-category {

  }
  .skill-category__title.skill-category__title {
    margin-top: 1.5rem;
    margin-bottom: 0.5rem;
  }
  .skill-category__content {
    margin-left: 0px;
    
  }
`;

const template = (skills: any[]) => html`
  <style>
    ${style}
  </style>
  <div class="skill-category">
    <h5 class="skill-category__title title is-italic is-5">${skills[0].category}</h5>
    <section class="skill-category__content">
      ${skills.map(s => {
        return html`
          <skill-metric data-skill="${s.skill}" data-score="${s.score}"></skill-metric>
        `;
      })}
    </section>
  </div>
`;

// class MyComponent extends HTMLElement { ... }
// window.customElements.define('my-component', MyComponent);
window.customElements.define(
  componentName,
  class extends HTMLElement {
    private _categorySkills: any[];

    constructor() {
      super();
      // Attribute Mapping
      this._categorySkills = JSON.parse(this.getAttribute("data-category-skills") || "{}");
      // An instance of the element is created or upgraded.
      // Useful for initializing state, setting up event listeners, or creating a shadow dom.
      // See the spec for restrictions on what you can do in the constructor.
    }

    public connectedCallback() {
      render(template(this._categorySkills), this);
      // Called every time the element is inserted into the DOM.
      // Useful for running setup code, such as fetching resources or rendering.
      // Generally, you should try to delay work until this time.
    }

    public disconnectedCallback() {
      // Called every time the element is removed from the DOM. Useful for running clean up code.
    }

    public static get observedAttributes() {
      return ["data-category-skills"];
      // Elements can react to attribute changes by defining a attributeChangedCallback.
      // The browser will call this method for every change to attributes listed in the observedAttributes array.
    }

    public attributeChangedCallback(attrName: string, oldVal: string, newVal: string) {
      switch (attrName) {
        case "data-category-skills":
          this._categorySkills = JSON.parse(newVal || "{}");
          return;
      }
    }

    public adoptedCallback() {
      // Called when an observed attribute has been added, removed, updated, or replaced.
      // Also called for initial values when an element is created by the parser, or upgraded.
      // Note: only attributes listed in the observedAttributes property will receive this callback.
      // The adoptedCallback is called each time the custom element is moved to a new document.
      // You'll only run into this use case when you have <iframe> elements in your page.
    }
  }
);
