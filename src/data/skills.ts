export default [
  {
    superCategory: "Frontend",
    category: "Vanilla",
    skill: "JS Browser API",
    score: 9
  },
  {
    superCategory: "Frontend",
    category: "Vanilla",
    skill: "CSS",
    score: 7
  },
  {
    superCategory: "Frontend",
    category: "Vanilla",
    skill: "Web Components",
    score: 6
  },
  {
    "-": "----------"
  },
  {
    superCategory: "Frontend",
    category: "Frameworks",
    skill: "React",
    score: 9
  },
  {
    superCategory: "Frontend",
    category: "Frameworks",
    skill: "Vue",
    score: 7
  },
  {
    superCategory: "Frontend",
    category: "Frameworks",
    skill: "Angular",
    score: 2
  },
  {
    "-": "----------"
  },
  {
    superCategory: "Frontend",
    category: "Practices",
    skill: "Page Speed Optimization",
    score: 9
  },
  {
    superCategory: "Frontend",
    category: "Practices",
    skill: "Responsive Design",
    score: 7
  },
  {
    superCategory: "Frontend",
    category: "Practices",
    skill: "Client Side Data Stores",
    score: 8
  },
  {
    superCategory: "Frontend",
    category: "Practices",
    skill: "Caching and Memoization",
    score: 8
  },
  {
    superCategory: "Frontend",
    category: "Practices",
    skill: "Project Architecture",
    score: 8
  },
  {
    superCategory: "Frontend",
    category: "Practices",
    skill: "CI / Ephemeral Deployments",
    score: 8
  },
  {
    "-": "--------------------------------------------------"
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Languages",
    skill: "Node / Typescript",
    score: 9
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Languages",
    skill: "Golang",
    score: 7
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Languages",
    skill: "Python",
    score: 5
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Languages",
    skill: "Java",
    score: 4
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Languages",
    skill: "C#",
    score: 4
  },
  {
    "-": "----------"
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Practices",
    skill: "Resource Pooling",
    score: 7
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Practices",
    skill: "Logging Patterns",
    score: 9
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Practices",
    skill: "Design Patterns",
    score: 6
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Practices",
    skill: "Object Oriented Programming",
    score: 6
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Practices",
    skill: "Data structures and Algorithms",
    score: 6
  },
  {
    superCategory: "Backend (Mid Tier)",
    category: "Practices",
    skill: "Solution Design and Architecture",
    score: 8
  },
  {
    "-": "--------------------------------------------------"
  },
  {
    superCategory: "Databases",
    category: "Theory",
    skill: "Relational Design",
    score: 9
  },
  {
    superCategory: "Databases",
    category: "Theory",
    skill: "No SQL Design",
    score: 8
  },
  {
    superCategory: "Databases",
    category: "Theory",
    skill: "Performance Tuning / Index Strategies",
    score: 8
  },
  {
    superCategory: "Databases",
    category: "Practices",
    skill: "Extract Transform Loads",
    score: 7
  },
  {
    superCategory: "Databases",
    category: "Practices",
    skill: "Complex Querying / Reporting",
    score: 9
  },
  {
    superCategory: "Databases",
    category: "Practices",
    skill: "Data Retention and Disaster Recovery",
    score: 9
  },
  {
    "-": "--------------------------------------------------"
  },
  {
    superCategory: "Databases",
    category: "Engines",
    skill: "MS SQL Server",
    score: 9
  },
  {
    superCategory: "Databases",
    category: "Engines",
    skill: "Postgres",
    score: 8
  },
  {
    superCategory: "Databases",
    category: "Engines",
    skill: "Dynamo DB",
    score: 7
  },
  {
    superCategory: "Databases",
    category: "Engines",
    skill: "MongoDB",
    score: 4
  },
  {
    superCategory: "Databases",
    category: "Engines",
    skill: "Redis",
    score: 4
  },
  {
    "-": "--------------------------------------------------"
  },
  {
    superCategory: "Dev Ops",
    category: "Tooling",
    skill: "Serverless Framework",
    score: 8
  },
  {
    superCategory: "Dev Ops",
    category: "Tooling",
    skill: "Docker",
    score: 7
  },
  {
    superCategory: "Dev Ops",
    category: "Tooling",
    skill: "Jenkins",
    score: 8
  },
  {
    superCategory: "Dev Ops",
    category: "Tooling",
    skill: "Terraform",
    score: 5
  },
  {
    superCategory: "Dev Ops",
    category: "Tooling",
    skill: "Shell Scripting",
    score: 9
  },
  {
    superCategory: "Dev Ops",
    category: "Tooling",
    skill: "Webpack",
    score: 9
  },
  {
    "-": "--------------------------------------------------"
  },
  {
    superCategory: "Cloud Platforms",
    category: "AWS",
    skill: "S3",
    score: 8
  },
  {
    superCategory: "Cloud Platforms",
    category: "AWS",
    skill: "Lambda",
    score: 9
  },
  {
    superCategory: "Cloud Platforms",
    category: "AWS",
    skill: "Step Functions / State Machines",
    score: 7
  },
  {
    superCategory: "Cloud Platforms",
    category: "AWS",
    skill: "SQS",
    score: 7
  },
  {
    superCategory: "Cloud Platforms",
    category: "AWS",
    skill: "IAM Role / Policy Setup",
    score: 6
  },
  {
    superCategory: "Cloud Platforms",
    category: "AWS",
    skill: "Cloud Watch",
    score: 10
  },
  {
    superCategory: "Cloud Platforms",
    category: "AWS",
    skill: "API Gateway",
    score: 6
  },
  {
    "-": "----------"
  },
  {
    superCategory: "Cloud Platforms",
    category: "Others",
    skill: "Kubernetes",
    score: 7
  },
  {
    superCategory: "Cloud Platforms",
    category: "Others",
    skill: "Google Cloud / Firebase",
    score: 6
  },
  {
    superCategory: "Cloud Platforms",
    category: "Others",
    skill: "Heroku",
    score: 5
  },
  {
    superCategory: "Cloud Platforms",
    category: "Others",
    skill: "Digital Ocean",
    score: 5
  }
];
