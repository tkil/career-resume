export default [
  "Played a key role in the development and success of Sell My Car",
  "Released re-launch of History Based Value",
  "Spearheaded the backend process for the upcoming speed dashboard",
  "Co-authored s3 ephemeral deploy process with Erik L.",
  "Lead on dev-ops and project setup for HBV, speed dashboard",
  "Seven years of experience as a Software Developer",
  "Four years of experience as a Database Analyst / Engineer",
  "Adapts quickly to new technologies, strategies and practices",
  "Firm believer in One Team CARFAX and works across teams",
  "Testing Champion",
  "Security Champion",
  "NPM Author",
  "Original member of the early VA dev team",
  "Flexible, team player and assists where needed"
];
